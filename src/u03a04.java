import java.util.Scanner;

public class u03a04 {
    public static void main(String[] args) {

        Scanner s = new Scanner(System.in);

        // Schaltjahrrechner

        System.out.println("Bitte beliebiges Jahr eingeben:");

        int j = s.nextInt();
        boolean b1, b2, b3, b4;
        b1 = (0 == j % 4);
        b2 = (0 != j % 100);
        b3 = (0 == j % 400);

        //1582

        b4 = true == b1 & (b2 | b3);

        System.out.println("Durch 4 restlos teilbar: " + b1);
        System.out.println("Nicht durch 100 teilbar: " + b2);
        System.out.println("Durch 400 restlos teilbar: " + b3);
        System.out.println(" ");
        System.out.println("Schaltjahr: " + b4);
        s.close();
    }
}
