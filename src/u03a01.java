/*
 * TODO Formulieren Sie f�r die Java-Operationen & (Logisches Und) und |
 * (Logisches Oder) das Kommutativgesetz, das Assoziativgesetz und das
 * Disributivgesetz.Wie lauten die de Morganschen Regeln?
 */

public class u03a01 {

    public static void main(String[] args) {

        int a, b, c;
        a = 3;
        b = 7;
        c = 5;
        boolean b1, b2, b3, b4, b5;

        // Kommutativgesetz
        b1 = a + c + b == a + b + c;
        b2 = b + a + c == c + a + b;
        b3 = b1 && b2;
        System.out.println(b3);

        // Assoziativgesetz
        b4 = a + (b + c) == (a + b) + c;
        System.out.println(b4);

        // Disributivgesetz
        b5 = a * (b + c) == a * b + a * c;
        System.out.println(b5);

    }

}
