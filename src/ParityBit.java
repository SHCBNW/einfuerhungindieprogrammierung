public class ParityBit {

    public static void main(String[] args) {

        int wert = 10111; // 0x17 = 10111
        int wert0 = wert;
        int quer = 0;
        int rest;
        boolean b1;

        while (wert > 0) {

            wert = wert / 10; // Letzte Zahl entfernen
            rest = wert % 10; // Letzte Zahl darstellen
            quer = quer + rest; // Addieren
        }

        b1 = quer % 2 == wert0 % 10; // (Quersumme=gerade?)=(Letzte Ziffer von Wert0)?

        System.out.println(b1);

        System.out.println(quer); // Quersumme

        System.out.println(quer % 2); // 0 gerade, 1 ungerade

    }

}
