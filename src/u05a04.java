import java.util.Scanner;

public class u05a04 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        // nach � in K�chenger�te
        // vor s in Wandschrank
        // vor a in Totalschaden

        String vornach, in, rest, ergebnisvor, ergebnisnach;
        int loop, indexvor, indexnach, len;
        char c;

        while (true) {

            System.out.println("Please enter kind of: (vor|nach) <c> in <string>");

            vornach = s.next();
            c = s.next().charAt(0);
            in = s.next();
            rest = s.next();

            indexvor = rest.indexOf(c);
            indexnach = rest.lastIndexOf(c);
            len = rest.length();

            if ((!vornach.equals("vor") & !vornach.equals("nach")) | !in.equals("in")
                    | (indexvor == -1 & indexnach == -1)) {

                System.out.println("Error. Please enter kind of: (vor|nach) <c> in <string>");

            } else {

                if (vornach.equals("vor")) {
                    ergebnisvor = rest.substring(0, indexvor);
                    System.out.println(ergebnisvor);

                } else {

                    ergebnisnach = rest.substring(indexnach + 1, len);
                    System.out.println(ergebnisnach);

                }
            }
            System.out.println();
        }
    }
}