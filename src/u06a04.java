/*
Schreiben Sie ein Java-Programm, das als Kommandozeilenargument den Namen einer Datei annimmt.

Das Programm soll ausgeben, wieviele Zeichen, die keine Leerzeichen sind, die Datei enth�lt.
Hinweis: Verwenden Sie die Methode Scanner.hasNext(), um festzustellen, ob die Datei noch weitere Zeichenketten
aus Nicht-Leerzeichen enth�lt. Diese werden Ihnen jeweils durch Scanner.next() geliefert.
*/

import java.io.IOException;
import java.io.RandomAccessFile;

public class u06a04 {
    public static void main(String[] args) {
        RandomAccessFile text;
        try {
            text = new RandomAccessFile("C:/Users/Siu/Desktop/text.txt", "r");
            for (String row; (row = text.readLine()) != null; ) {
                System.out.println(row);
                int len = row.length();
                int counter = 0;

                for (int i = 0; i <= len - 1; i++) {
                    char sub = row.charAt(i);
                    if (sub != ' ')
                        counter++;
                }
                System.out.println(counter);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
