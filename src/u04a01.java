public class u04a01 {

    public static void main(String[] args) {
        // 1. i, j und k sind alle echt kleiner als 50
        // 2. die Summe von i,j und k ist nicht durch 7 teilbar
        // 3. j ist ungerade und ist entweder im Intervall [3,21] oder im
        // Intervall [112,157]
        // 4. k ist weder durch 3 noch durch 7 teilbar, aber gerade

        int i, j, k;
        boolean w, x, y, z;

        i = 5;
        j = 20;
        k = 20;

        // 1
        w = (i < 50) & (j < 50) & (k < 50);
        System.out.println(w);

        // 2
        x = 0 != (i + j + k) % 7;
        System.out.println(x);

        // 3
        y = (j % 2 != 0) & ((j >= 3 & j <= 21) | (j >= 112 & j <= 157));
        System.out.println(y);

        // 4
        z = (k % 3 != 0) & (k % 7 != 0) & (k % 2 == 0);
        System.out.println(z);
    }

}
