import java.util.Scanner;

public class LaengsteKette {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        String entry = s.nextLine();
        int len, counter, longest;
        char checker, sub;

        len = entry.length();
        checker = 0;
        counter = 0;
        longest = 1;

        for (int i = 0; i <= len - 1; i++) {

            sub = entry.charAt(i);

            // System.out.println(sub);

            if (!(sub == ' ' | sub == '.')) {
                if (sub != checker) {
                    checker = sub;
                    counter = 1;
                    // System.out.println("checker=" + checker);
                } else {
                    counter++;
                    // System.out.println("counter++ | = " + counter);
                    if (counter >= longest) {
                        longest = counter;
                    }
                }
            }
        }
        System.out.println(longest);
        s.close();
    }
}