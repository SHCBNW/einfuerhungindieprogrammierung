public class u06a01 {
    public static void main(String[] args) {
        int n, m, sum1, sum2, sum3;
        n = Integer.parseInt(args[0]);
        m = Integer.parseInt(args[1]);

        System.out.println("n=" + n);
        System.out.println("m=" + m);

        if (n >= m) {
            System.out.println("Fehler! n muss kleiner sein als m.");
        } else {

            // A FOR Z�hlerschleife
            sum1 = n;
            for (int i = n + 1; i <= m; i++) {
                sum1 = sum1 + i;
            }
            System.out.println("A: " + sum1);

            // B WHILE Kopfgesteuerte Schleife
            int counter = n;
            sum2 = n;
            while (counter != m) {
                counter++;
                sum2 = sum2 + counter;
            }
            System.out.println("B: " + sum2);

            // C DO-WHILE Fu�gesteuerte Schleife
            int counter2 = n;
            sum3 = n;
            do {
                counter2++;
                sum3 = sum3 + counter2;
            } while (counter2 != m);
            System.out.println("C: " + sum3);
        }
    }
}
