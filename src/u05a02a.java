import java.util.Scanner;

/*
 * Schreiben Sie drei Java-Programme, die jeweils drei ganze Zahlen addieren und das Ergebnis auf dem Bildschirm ausgeben.
Die Programme sollen sich in der Eingabe der drei Zahlen unterscheiden: Eingabe �ber die Kommandozeile, Eingabe
�ber die Tastatur w�hrend des Programmlaufs und Eingabe aus einer Datei.
Hinweis: Schauen Sie sich auf der Java-Seite (ganz unten) die Hinweise und Beispiele zum Einlesen vonWerten an.
 */

public class u05a02a {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        int a, b, c, d, e, f;

        a = s.nextInt();
        b = s.nextInt();
        c = s.nextInt();
        s.close();

        System.out.println(a + b + c);

        d = Integer.parseInt(args[0]);
        e = Integer.parseInt(args[1]);
        f = Integer.parseInt(args[2]);

        System.out.println(d + e + f);
    }
}
