public class schaltjahr {

    public static void main(String[] args) {

        int j;
        boolean r1, r2, r3, r, r4;
        j = 2020;
        r1 = 0 == (j % 4);
        r2 = 0 != (j % 100);
        r3 = 0 != (j % 400);
        r4 = 0 == (j % 1000);

        r = true == r1 && (r2 && r3 || r4);

        System.out.print("Durch 4 teilbar: ");
        System.out.println(r1);
        System.out.print("Durch 100 nicht teilbar: ");
        System.out.println(r2);
        System.out.print("Durch 400 nicht teilbar: ");
        System.out.println(r3);
        System.out.print("Durch 1000 teilbar: ");
        System.out.println(r4);

        System.out.print(j + " ist ein Schaltjahr: ");
        System.out.print(r);

    }

}
