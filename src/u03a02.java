public class u03a02 {
    public static void main(String[] args) {
        int a;
        a = 4;

        boolean u, w, x;
        x = true;

        // 1. (x < 100) | !(x < 200)
        w = a < 100 | a >= 200;
        System.out.println(w);

        // 2. x & !(y & !(z | y))
        u = x;
        System.out.println(u);

    }
}
