//u03a06

public class BoolExpression {
    public static void main(String[] args) {
        boolean wert1, wert2, b;
        wert1 = false;
        wert2 = true;
        //noinspection ConstantConditions
        b = !wert1 & (wert1 | wert2);
        //noinspection ConstantConditions
        System.out.println(b);
        wert1 = true;
        //noinspection ConstantConditions
        b = !wert1 & (wert1 | wert2);
        //noinspection ConstantConditions
        System.out.println(b);
    }
}
