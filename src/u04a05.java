public class u04a05 {

    public static void main(String[] args) {
        // TODO Auto-generated method stub

        boolean b1, b2, b3, b4, b5, b6;

        b1 = true;
        b2 = false;

        // 1. !wert1 & (wert1 | wert2)
        b3 = !b1 && (b1 | b2);
        System.out.println("!b1 && (b1 | b2) " + b3);

        // 2. (wert1 & wert2) | !(wert1 | wert2)
        b4 = (b1 & b2) | !(b1 | b2);
        System.out.println(b4);

        // 3. !(wert1 & wert2) | (!wert1 | !wert2)
        b5 = !(b1 & b2) | (!b1 | !b2);
        System.out.println(b5);

        // 4. !(!wert1 | false) & (wert1 != wert2)
        b6 = !(!b1 | b2) & (b1 != b2);
        System.out.println(b6);

    }
}
