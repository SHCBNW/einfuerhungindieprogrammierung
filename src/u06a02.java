public class u06a02 {
    public static void main(String[] args) {
        double x = Double.parseDouble(args[0]);
        double y = Double.parseDouble(args[1]);
        double e = Double.parseDouble(args[2]);
        double a, b;

        //noinspection InfiniteLoopStatement
        while (true) {
            int counter = 0;
            if ((x > y) | (e < 0) | (Math.abs(y - x) < e)) {
                System.out.println("Error. Zahlen in der Komandozeile nicht korrekt.");
            } else {
                do {
                    a = (y - x) * Math.random() + x;
                    b = (y - x) * Math.random() + x;
                    System.out.println("a= " + a);
                    System.out.println("b= " + b);
                    System.out.println();
                    counter++;
                } while (Math.abs(a - b) <= e);
                System.out.println("Gegeben: " + x + " - " + y + " und e ist " + e);
                System.out.println((Math.round(Math.abs(a - b))) + " (|a-b|) ist gr��er als e.");
                System.out.println(counter + " Versuche ben�tigt.");
            }
            System.out.println();
            System.out.println("Press enter to continue...");
            try {
                //noinspection ResultOfMethodCallIgnored
                System.in.read();
            } catch (Exception ignored) {
            }
        }
    }
}