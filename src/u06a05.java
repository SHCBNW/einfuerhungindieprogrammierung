public class u06a05 {
    public static void main(String[] args) {
        int basis, wert, zwischenergebnis, zwischenbasis, ergebnis, counter;
        String check = args[1];
        String alph = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        basis = Integer.parseInt(args[0]);
        counter = 0;
        zwischenbasis = 1;
        ergebnis = 0;

        for (int i = check.length() - 1; i >= 0; i--) {
            char a = check.charAt(i);
            wert = alph.indexOf(a);
            if (wert == -1) {
                System.out.println("Das Zeichen '" + a + "' wird �bersprungen.");
            } else {
                System.out.println(a + " hat die " + wert + "te Stelle");
                if (counter == 0) {
                    zwischenergebnis = wert;
                    counter++;
                } else {
                    zwischenbasis = zwischenbasis * basis;
                    // System.out.println("zwbasis " + zwischenbasis);
                    zwischenergebnis = wert * zwischenbasis;
                }
                ergebnis += zwischenergebnis;
                System.out.println("Zwischenergebnis= " + ergebnis);
            }
            System.out.println();
        }
        System.out.println(check + " = " + ergebnis + "(" + basis + ")");
    }
}
