import java.util.Scanner;

public class Caesar {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int k = s.nextInt();
        char b = s.next().charAt(0);
        char out = (char) ((k + b - 'A') % 26 + 'A');

        System.out.println(out);
        s.close();
    }
}